# build-with-llvm-tool

Python scripts that provide a drop-in solution that will
apply an LLVM Pass to the build process of existing code.

You must set the following variables for the procedure to
work properly:

export LLVM_TOOL_NAME="Tool_Name"
export LLVM_ROOT="/path/to/llvm/build"