import sys,os,subprocess
from enum import Enum

if not 'noopt' in globals():
    noopt = False

class Mode(Enum):
    SOURCE = 1
    OBJECT = 2
    EXE = 3

def is_source_file(f):
    return f.endswith(".c") or f.endswith(".cc") or f.endswith(".cpp")
    
def rewrite_extension(f,new):
    idx = f.rfind(".")
    if idx == -1:
        return f
    return f[0:idx] + new

def rewrite_extension_list(files,new):
    output = []
    for f in files:
        output.append(rewrite_extension(f,new))
    return output
    
def compile(input_file, output_file, options, libs):
    print "Compiling"
    # First command emits LLVM IR and compiles the source file to .bc
    command = [LLVM_BIN + compiler,"-emit-llvm"]
    command.extend(options)

    print options
    
    if not "-c" in options:
        command.append("-c")
    
    command.append(input_file)
    
    command.append("-o")

    if not output_file or output_file == "":
        output_file = rewrite_extension(input_file,".bc")
    command.append(output_file)

    print command
    
    print ' '.join(command)
    subprocess.call(command)

    # Second command runs opt on the .bc file to instrument the IR
    if noopt:
        instr_file = rewrite_extension(output_file,"_instr.bc")
        print "cp " + output_file + " " + instr_file
        os.system("cp " + output_file + " " + instr_file)
    else:
        instr_file = rewrite_extension(output_file,"_instr.bc")

        command = LLVM_BIN + OPT_COMMAND + " < "
        command = command + output_file
        command = command + " > " + instr_file
        print command
        os.system(command)

def link(input_files, output_file, options, libs):
    # Different command depending on if we're building for debug or inlining for performance
    if link_lib == "" and noopt == False:
        command = [LLVM_BIN + "llvm-link","-o","instrumented.bc",link_bc]
    else:
        command = [LLVM_BIN + "llvm-link","-o","instrumented.bc"]

    command.extend(input_files)
    print ' '.join(command)
    subprocess.call(command)

    file_in = "instrumented.bc"
    if link_lib == "" and noopt == False:
        command = LLVM_BIN + "opt -inline < instrumented.bc > opt.bc"
        print command
        os.system(command)
        file_in = "opt.bc"

    command = [LLVM_BIN + "llc",file_in,"-o","instrumented.s"]
    print ' '.join(command)
    subprocess.call(command)

    command = [LLVM_BIN +  "clang++","instrumented.s"]
    if EXTRA_LIBS:
        command.extend(EXTRA_LIBS)
    if link_lib:
        command.append(link_lib)
    if options:
        command.extend(options)
    command.append("-o");
    command.append(output_file)
    if libs:
        command.extend(libs)
    print command
    print ' '.join(command)
    subprocess.call(command)

def determine_input_mode(args):
    mode = Mode.OBJECT
    for arg in args:
        if is_source_file(arg):
            mode = Mode.SOURCE
    return mode

def determine_output_mode(args):
    mode = Mode.EXE
    checkNext = False
    for arg in args:
        if checkNext:
            if arg.endswith(".o"):
                return Mode.OBJECT
            checkNext = False
        if arg == "-o":
            checkNext = True
        if arg == "-c":
            return Mode.OBJECT
            
    return mode
            
    
def find_output_file(output_mode,args):
    checkNext = False
    for arg in args:
        if checkNext:
            return arg
        if arg == "-o":
            checkNext = True
    
def find_input_files(input_mode,args):
    input_files = []
    if input_mode == Mode.SOURCE:
        for arg in args:
            if is_source_file(arg):
                input_files.append(arg)
    else:
        for arg in args:
            if arg.endswith(".o"):
                input_files.append(arg)
    return input_files

def find_libraries(args):
    libs = []
    for arg in args:
        if arg.startswith("-l"):
            libs.append(arg)
            continue

        if arg.startswith("-L"):
            libs.append(arg)
            continue
    return libs

# Find any command line options that aren't covered by other cases
def find_option_args(args):
    options = []
    skipNext = False
    for arg in args:
        if skipNext:
            skipNext = False
            continue
        if arg.endswith(".c"):
            continue
        if arg.endswith(".cc"):
            continue
        if arg.endswith(".cpp"):
            continue
        if arg.endswith(".o"):
            continue
        if arg == "-o":
            skipNext = True
            continue
        if arg == "-l":
            skipNext = True
            continue
        if arg == "-L":
            skipNext = True
            continue
        options.append(arg)
    return options
    
args = sys.argv[1:]

input_mode = determine_input_mode(args)
output_mode = determine_output_mode(args)

input_files = find_input_files(input_mode,args)
output_file = find_output_file(output_mode,args)

libs = find_libraries(args)

options = find_option_args(args)

if input_mode == Mode.SOURCE and output_mode == Mode.EXE:
    for input_file in input_files:
        compile(input_file,"",options,libs)
    # TODO: Rewrite input files to .o before linking
    link_files = rewrite_extension_list(input_files,"_instr.bc")
    link(link_files, output_file, options, libs)
elif input_mode == Mode.SOURCE and output_mode == Mode.OBJECT:
    for input_file in input_files: 
        compile(input_file,output_file,options,libs)
elif input_mode == Mode.OBJECT and output_mode == Mode.EXE:
    input_files = rewrite_extension_list(input_files,"_instr.bc")
    link(input_files, output_file, options, libs)
