#!/usr/bin/python

import os

SCRIPT_ROOT = os.environ["LLVM_SCRIPT_ROOT"]
LLVM_BIN = os.environ["LLVM_BIN"]

execfile(SCRIPT_ROOT + "/config.py")

noopt = True
compiler = "clang"
link_lib = ""
link_bc = ""

execfile(SCRIPT_ROOT + "/compile.py")
